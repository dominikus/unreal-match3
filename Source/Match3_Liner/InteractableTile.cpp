// Fill out your copyright notice in the Description page of Project Settings.

#include "Kismet/GameplayStatics.h"
#include "Match3_LinerGameModeBase.h"
#include "InteractableTile.h"


void AInteractableTile::BeginPlay()
{
	Super::BeginPlay();

	OnClicked.AddUniqueDynamic(this, &AInteractableTile::OnTileSelected);
	OnBeginCursorOver.AddUniqueDynamic(this, &AInteractableTile::OnTileHovered);
}

void AInteractableTile::OnTileHovered(AActor* MousedOverActor)
{
	if (ATile* tile = Cast<ATile>(MousedOverActor))
	{
		if (AMatch3_LinerGameModeBase* gameMode = Cast<AMatch3_LinerGameModeBase>(UGameplayStatics::GetGameMode(this)))
		{
			gameMode->RegisterHoveredTile(tile);
		}
	}
}

void AInteractableTile::OnTileSelected(AActor* MousedOverActor, FKey ButtonClicked)
{
	if (ATile* tile = Cast<ATile>(MousedOverActor))
	{
		if (AMatch3_LinerGameModeBase* gameMode = Cast<AMatch3_LinerGameModeBase>(UGameplayStatics::GetGameMode(this)))
		{
			gameMode->RegisterClickedTile(tile);
		}
	}
}
