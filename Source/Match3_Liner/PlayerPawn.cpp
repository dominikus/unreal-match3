// Fill out your copyright notice in the Description page of Project Settings.

#include "Match3_LinerGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "PlayerPawn.h"


APlayerPawn::APlayerPawn()
{
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	UCameraComponent* Cam = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));
	Cam->SetupAttachment(RootComponent);
}

void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAction("LMB", EInputEvent::IE_Released, this, &APlayerPawn::MouseReleased);
}

void APlayerPawn::MouseReleased()
{
	if (AMatch3_LinerGameModeBase* gameMode = Cast<AMatch3_LinerGameModeBase>(UGameplayStatics::GetGameMode(this)))
	{
		gameMode->RegisterReleasedTile();
	}
}


