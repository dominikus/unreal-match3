// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Tile.h"

#include "InteractableTile.generated.h"

/**
 * Interactible tiles like gems, bombs, etc.
 */
UCLASS(Abstract)
class MATCH3_LINER_API AInteractableTile : public ATile
{
	GENERATED_BODY()
private:
protected:
	virtual void BeginPlay() override;

public:
UPROPERTY()
	FVector TempTarget; 

	UFUNCTION()
		void OnTileHovered(AActor* MousedOverActor);
	UFUNCTION()
		void OnTileSelected(AActor* MousedOverActor, FKey ButtonClicked);
};