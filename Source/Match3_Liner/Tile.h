// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

/**
 * Basic Tile with no functionality
 */
UCLASS(Abstract)
class MATCH3_LINER_API ATile : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	UStaticMeshComponent* MeshComp;
	UPROPERTY()
	UMaterial* StoredMaterial;

public:
	UPROPERTY()
	UMaterialInstanceDynamic* DynamicMaterialInst;

	UPROPERTY(EditAnywhere, Category = Match3)
		bool bSelectable = true;
};
