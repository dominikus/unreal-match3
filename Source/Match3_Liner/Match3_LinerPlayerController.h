// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Match3_LinerPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MATCH3_LINER_API AMatch3_LinerPlayerController : public APlayerController
{
	GENERATED_BODY()
protected:
	
public:
	AMatch3_LinerPlayerController();

};
