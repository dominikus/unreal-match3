// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "GemTile.h"
#include "Grid.h"
#include "Match3_LinerPlayerController.h"
#include "PlayerPawn.h"

#include "Match3_LinerGameModeBase.generated.h"

/**
 * GameMode who manages the whole game.
 */
UCLASS()
class MATCH3_LINER_API AMatch3_LinerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
private:
	bool SpawnGrid(UWorld* world);
	void GameOver();
	void RestartGame();
	void StartSelection();
	void AbortSelection();
	void ProcessMatch();

	/// <summary>
	/// Sets the emission of a tiles material to provide a visual feedback about the selection process
	/// </summary>
	/// <param name="Tile">The tile to highlite or disable</param>
	/// <param name="bOn">Switch</param>
	void SwitchTileGlow(ATile* Tile, bool bOn);

protected:
	UPROPERTY()
		AGrid* Grid;
	
	/** A not yet implemented score. Just add for every destroyed Gem some Score and display it. */
	UPROPERTY()
		int32 Score;

	UPROPERTY()
		AMatch3_LinerPlayerController* PlayerController;
	UPROPERTY()
		APlayerPawn* PlayerPawn;

	/** Tiles that are currently selected. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<AInteractableTile*> CurrentlySelectedTiles;

	uint8 currentTurn;
	TEnumAsByte<EGemType::Type> CurrentSelectionType;
	FTimerHandle GameOverTimer;

	// ~ Game Settings 

	/** Decide the minimum amount of connecting tiles for a successful match */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Match3)
		uint8 MinRunLenght = 3;
	/** Maximum amount of turns until Game-Over */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Match3)
		uint8 MaxTurns = 10;
	/** Amount of time per game until Game-Over (Needs an UI)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Match3)
		float Timelimit = 30.0f;

	// ~ Game Settings 

public:
	AMatch3_LinerGameModeBase();
	virtual void BeginPlay() override;

	/** I tried to create some tile animations, unfortunately I didn't get it to work well enough */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Match3)
		bool bUseAnimations = true;

	/// <summary>
	/// Turns off the whole user input while animating the dropping tiles.
	/// </summary>
	/// <param name="bOn">On = true = enabled user input</param>
	void SwitchUserInput(bool bOn);

	void RegisterHoveredTile(ATile* HoveredTile);
	void RegisterClickedTile(ATile* ClickedTile);
	void RegisterReleasedTile();
};
