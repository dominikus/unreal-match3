// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawn.generated.h"

/**
 * Pawn class solely to have a spawn point and a global mouse-released receiver
 */
UCLASS()
class MATCH3_LINER_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	APlayerPawn();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** The mouse release has to be catched globally, not only on a interactable tile. */
	void MouseReleased();
};
