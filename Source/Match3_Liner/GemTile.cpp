// Fill out your copyright notice in the Description page of Project Settings.


#include "GemTile.h"

AGemTile::AGemTile() : AInteractableTile()
{
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CoinMeshComp"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundMesh(TEXT("/Game/Meshes/Coin"));
	if (FoundMesh.Succeeded())
	{
		MeshComp->SetStaticMesh(FoundMesh.Object);
	}
	SetRootComponent(MeshComp);
	MeshComp->SetMobility(EComponentMobility::Movable);

	static ConstructorHelpers::FObjectFinder<UMaterial> FoundMaterial(TEXT("/Game/BaseMat"));
	if (FoundMaterial.Succeeded())
	{
		StoredMaterial = FoundMaterial.Object;
	}
	DynamicMaterialInst = UMaterialInstanceDynamic::Create(StoredMaterial, MeshComp);

	RandomizeType();

	MeshComp->SetMaterial(0, DynamicMaterialInst);
}

void AGemTile::RandomizeType()
{
	GemType = static_cast<EGemType::Type>(FMath::RandRange(0, EGemType::NUM - 1));
	switch (GemType)
	{
	case EGemType::Blue:
		DynamicMaterialInst->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor::Blue);
		break;
	case EGemType::Red:
		DynamicMaterialInst->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor::Red);
		break;
	case EGemType::Yellow:
		DynamicMaterialInst->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor::Yellow);
		break;
	case EGemType::Green:
		DynamicMaterialInst->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor::Green);
		break;
	default:
		break;
	}
}
