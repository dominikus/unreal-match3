// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractableTile.h"
#include "GemTile.generated.h"

UENUM()
namespace EGemType
{
	enum Type
	{
		Green,
		Blue,
		Red,
		Yellow,
		NUM			UMETA(Hidden)
	};
}


/**
 * The basic tile for this game. Type is defined through EGemType
 */
UCLASS()
class MATCH3_LINER_API AGemTile : public AInteractableTile
{
	GENERATED_BODY()
private:

protected:

public:
	AGemTile();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Match3)
		TEnumAsByte<EGemType::Type> GemType;

	void RandomizeType();
};
