// Copyright Epic Games, Inc. All Rights Reserved.

#include "Match3_LinerGameModeBase.h"
#include "Kismet/GameplayStatics.h"


AMatch3_LinerGameModeBase::AMatch3_LinerGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;

	PlayerControllerClass = AMatch3_LinerPlayerController::StaticClass();
	DefaultPawnClass = APlayerPawn::StaticClass();

	currentTurn = MaxTurns;
}

void AMatch3_LinerGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	UWorld* const world = GetWorld();
	if (world)
	{
		PlayerController = Cast<AMatch3_LinerPlayerController>(world->GetFirstPlayerController());
		PlayerPawn = Cast<APlayerPawn>(PlayerController->GetPawn());
		PlayerPawn->AddActorWorldRotation(FRotator(270.f, 0.f, 0.f));

		if(!SpawnGrid(world))
			UE_LOG(LogTemp, Error, TEXT("Failed to spawn Grid!"));
		
		GetWorldTimerManager().SetTimer(GameOverTimer, this, &AMatch3_LinerGameModeBase::GameOver, Timelimit, false);
	}
}

bool AMatch3_LinerGameModeBase::SpawnGrid(UWorld* world)
{
	FActorSpawnParameters GridSpawnParameters;
	GridSpawnParameters.Owner = this;
	GridSpawnParameters.Instigator = GetInstigator();
	GridSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	Grid = world->SpawnActor<AGrid>(AGrid::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, GridSpawnParameters);
	if(!IsValid(Grid))
		return false;
	
	if(!Grid->InitGrid())
		return false;

	return true;
}

void AMatch3_LinerGameModeBase::GameOver()
{
	// Save Score in Highscore
	RestartGame();
}

void AMatch3_LinerGameModeBase::RestartGame()
{
	GetWorldTimerManager().ClearTimer(GameOverTimer);
	currentTurn = MaxTurns;
	Score = 0;
	Grid->ShuffleGrid();

	GetWorldTimerManager().SetTimer(GameOverTimer, this, &AMatch3_LinerGameModeBase::GameOver, Timelimit, false);
}

void AMatch3_LinerGameModeBase::StartSelection()
{
	CurrentlySelectedTiles.Empty();
}

void AMatch3_LinerGameModeBase::AbortSelection()
{
	// Turn off the glow of every selected tile
	for(AInteractableTile* tile : CurrentlySelectedTiles)
		SwitchTileGlow(tile, false);

	CurrentlySelectedTiles.Empty();
}

void AMatch3_LinerGameModeBase::ProcessMatch()
{
	currentTurn--;
	
	for (AInteractableTile* tile : CurrentlySelectedTiles)
	{
		SwitchTileGlow(tile, false);
	}
	Grid->DeletesTileAndSpawnNew(CurrentlySelectedTiles);

	// Add Score

	CurrentlySelectedTiles.Empty();

	if (currentTurn <= 0)
	{
		GameOver();
	}
}

void AMatch3_LinerGameModeBase::SwitchUserInput(bool bOn)
{
	bOn ? EnableInput(PlayerController) : DisableInput(PlayerController);
}

void AMatch3_LinerGameModeBase::SwitchTileGlow(ATile* Tile, bool bOn)
{
	bOn ? Tile->DynamicMaterialInst->SetScalarParameterValue(FName(TEXT("Emission")), 0.5f)
		: Tile->DynamicMaterialInst->SetScalarParameterValue(FName(TEXT("Emission")), 0.0f);
}

void AMatch3_LinerGameModeBase::RegisterHoveredTile(ATile* HoveredTile)
{
	if (CurrentlySelectedTiles.Num() <= 0)
		return;

	if(AInteractableTile* interactableTile = Cast<AInteractableTile>(HoveredTile))
	{
		// New Tile is a gem
		if (AGemTile* gem = Cast<AGemTile>(interactableTile))
		{
			if (CurrentSelectionType > 0 && CurrentSelectionType < EGemType::NUM)	// Gemtype fits
			{
				if (CurrentlySelectedTiles.Contains(gem))	// Gem is already in the list -> user is moving back over the already selected ones
				{
					if (CurrentlySelectedTiles.Num() > 1)
					{
						if (CurrentlySelectedTiles[(CurrentlySelectedTiles.Num() - 2)] == gem)	// if user moved back in order -> remove selected gem from selectedlist 
						{
							SwitchTileGlow(CurrentlySelectedTiles.Top(), false);
							CurrentlySelectedTiles.Pop();
						}
						else	// User went back in a different order -> Abort
							AbortSelection();
					}
				}
				else	// a not yet registered gem in the selectedlist
				{
					CurrentlySelectedTiles.Push(gem);
					SwitchTileGlow(gem, true);
				}
			}
			else	// CurrentSelectionType is not valid, hence this is the first valid selected gem (but not the first selected interactable tile)
			{
				CurrentSelectionType = gem->GemType;
				CurrentlySelectedTiles.Push(gem);
				SwitchTileGlow(gem, true);
			}
		}
		else	// Any other intractable tile
		{
			SwitchTileGlow(interactableTile, true);
			CurrentlySelectedTiles.Push(interactableTile);
		}
	}
	// Not a intractable tile: destroy if the already selected ones are more than minrunlenght or abort selecting process	
	//TODO: To be removed, cause the interactable tiles are the only ones that trigger inputs
	else
	{
		if (CurrentlySelectedTiles.Num() >= MinRunLenght)
			ProcessMatch();
		else
			AbortSelection();
	}
}

void AMatch3_LinerGameModeBase::RegisterClickedTile(ATile* ClickedTile)
{
	if (ClickedTile->bSelectable)
	{
		if (AInteractableTile* interactableTile = Cast<AInteractableTile>(ClickedTile))
		{
			StartSelection();
			if (AGemTile* gem = Cast<AGemTile>(interactableTile))
			{
				CurrentSelectionType = gem->GemType;
			}			
			CurrentlySelectedTiles.Add(interactableTile);
			SwitchTileGlow(interactableTile, true);
		}
	}
}

void AMatch3_LinerGameModeBase::RegisterReleasedTile()
{
	if (CurrentlySelectedTiles.Num() >= MinRunLenght)
		ProcessMatch();
	else
	{
		AbortSelection();
	}
}
