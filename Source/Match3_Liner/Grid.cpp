// Fill out your copyright notice in the Description page of Project Settings.

#include "GemTile.h"
#include "BaseTile.h"
#include "Match3_LinerGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Grid.h"



AGrid::AGrid()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

bool AGrid::InitGrid()
{
	if (GridRows == 0 || GridColumns == 0)
	{
		UE_LOG(LogTemp, Error, TEXT("InitGrid failed: Rows and Columns can't be 0"));
		return false;
	}

	// Reserve one more row for the top to spawn new tiles
	BaseTiles.Reserve(GridColumns * (GridRows + 1));
	Tiles.Reserve(GridColumns * GridRows);
	FallingTilesMap.Reserve(GridColumns * GridRows);

	for (int z = 0; z < GridColumns; z++)
	{
		for (int x = 0; x < GridRows + 1; x++)
		{
			float xOffset = TileOffset.X * (x + z * 0.5f - z / 2);
			float yOffset = TileOffset.Y * z;
			FVector newBasePos = GetActorLocation() + FVector(xOffset, yOffset, -TileOffset.Z);
			FVector newTilePos = GetActorLocation() + FVector(xOffset, yOffset, 0);

			if (ATile* newTile = SpawnTileOfType(ABaseTile::StaticClass(), newBasePos)) {}
			else
				UE_LOG(LogTemp, Log, TEXT("Failed to spawn Base!"));

			// Stop one row earlier for the GemTiles
			if(x < GridRows)
			{
				if (ATile* newTile = SpawnTileOfType(AGemTile::StaticClass(), newTilePos)) {}
				else
					UE_LOG(LogTemp, Log, TEXT("Failed to spawn Tile!"));
			}
		}
	}

	return true;
}

ATile* AGrid::SpawnTileOfType(TSubclassOf<class ATile> TileType, FVector SpawnLocation)
{
	if (TileType)
	{
		if (UWorld* const world = GetWorld())
		{
			FActorSpawnParameters TileSpawnParameters;
			TileSpawnParameters.Owner = this;
			TileSpawnParameters.Instigator = GetInstigator();
			TileSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			ATile* NewTile = world->SpawnActor<ATile>(TileType, SpawnLocation, FRotator::ZeroRotator, TileSpawnParameters);
			NewTile->GetRootComponent()->SetMobility(EComponentMobility::Movable);

			if(Cast<ABaseTile>(NewTile))
				BaseTiles.Add(NewTile);
			else if (Cast<AInteractableTile>(NewTile))
				Tiles.Add(NewTile);

			return NewTile;
		}
	}
	return nullptr;
}

void AGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FallingTimeline.TickTimeline(DeltaTime);
}

void AGrid::BeginPlay()
{
	Super::BeginPlay();

	m_GameMode = Cast<AMatch3_LinerGameModeBase>(UGameplayStatics::GetGameMode(this));
	if (!IsValid(m_GameMode))
		UE_LOG(LogTemp, Warning, TEXT("GameMode is not set in the grid"));

	// Create Curve
	FRichCurve* richCurve = new FRichCurve();
	richCurve->AddKey(0.0f, 0.f);
	richCurve->AddKey(0.5f, 0.3f);
	richCurve->AddKey(1.0f, 1.f);

	Curve = NewObject<UCurveFloat>();
	Curve->FloatCurve = *richCurve;
	delete richCurve;

	// Add Callbacks
	FOnTimelineFloat TimelineCallback;
	FOnTimelineEventStatic TimelineFinishedCallback;

	TimelineCallback.BindUFunction(this, FName("FallingDown"));
	TimelineFinishedCallback.BindUFunction(this, FName{ TEXT("FallingFinished") });
	FallingTimeline.AddInterpFloat(Curve, TimelineCallback);
	FallingTimeline.SetTimelineFinishedFunc(TimelineFinishedCallback);
}

void AGrid::FallingFinished()
{
	for(TMap<AInteractableTile*, uint8>::TIterator It = FallingTilesMap.CreateIterator(); It; ++It)
	{
		if (It.Value() <= 1)
		{
			It.RemoveCurrent();
		}
		else
		{
			It.Value()--;
			It.Key()->TempTarget = It.Key()->GetActorLocation() + FVector(-TileOffset.X, 0.f, 0.f);
		}
		// If I want to remove the current pair for whatever reason.
	}
	FallingTilesMap.Compact();

	if(FallingTilesMap.Num() > 0)
		FallingTimeline.PlayFromStart();
	else
		m_GameMode->SwitchUserInput(true);

	//FallingTilesMap.Empty(GridColumns * GridRows);
}

void AGrid::StartFalling()
{
	m_GameMode->SwitchUserInput(false);
	FallingTimeline.PlayFromStart();
}

void AGrid::FallingDown()
{
	float TimelineValue = FallingTimeline.GetPlaybackPosition();
	float CurveFloatValue = Curve->GetFloatValue(TimelineValue);

	for (auto fallingTileTuple : FallingTilesMap)
	{
		fallingTileTuple.Key->SetActorLocation(FMath::Lerp(fallingTileTuple.Key->GetActorLocation(), fallingTileTuple.Key->TempTarget, CurveFloatValue));
	}
}

uint8 AGrid::GetColumn(ATile* tile)
{
	return ((Tiles.Find(tile)) / GridRows);
}

uint8 AGrid::GetRow(ATile* tile)
{
	return ((Tiles.Find(tile)) % GridRows);
}

void AGrid::DeletesTileAndSpawnNew(TArray<AInteractableTile*> tilesToDelete)
{
	/** Move the destroyed tile up and put every tile that has to update its position in the falling tiles map */
	for (AInteractableTile* tileToDelete : tilesToDelete)
	{
		uint8 additionalRows = 0;
		if (FallingTilesMap.Contains(Cast<AInteractableTile>(tileToDelete)))
			additionalRows = FallingTilesMap[Cast<AInteractableTile>(tileToDelete)];

		int32 column = GetColumn(tileToDelete);
		int32 row = GetRow(tileToDelete);
		float newXOffset = TileOffset.X * (GridRows + additionalRows + column * 0.5f - column / 2);
		float newYOffset = TileOffset.Y * column;
		FVector newTilePos = GetActorLocation() + FVector(newXOffset, newYOffset, 0);

		Cast<AGemTile>(tileToDelete)->RandomizeType();	// Set random type
		tileToDelete->SetActorLocation(newTilePos);		// Set the "destroyed" tile on top of the column

		
		for (int i = row; i < GridRows - 1; i++)	// Swap the "destroyed" Tile up to keep the index locations for the calculations.
		{
			Tiles.Swap(column * GridRows + i, column * GridRows + i + 1);
		}

		if (m_GameMode->bUseAnimations)
		{
			for (int i = row; i < GridRows; i++)
			{
				int8 index = column * GridRows + i;
				if(FallingTilesMap.Contains(Cast<AInteractableTile>(Tiles[index])))
					FallingTilesMap[Cast<AInteractableTile>(Tiles[index])]++;
				else
					FallingTilesMap.Add(Cast<AInteractableTile>(Tiles[index]), 1);
			}
		}
		else
		{
			for (int i = row; i < GridRows; i++)
			{
				FVector currentPos = Tiles[column * GridRows + i]->GetActorLocation();
				newTilePos = FVector(currentPos.X - TileOffset.X, currentPos.Y, currentPos.Z);
				Cast<AInteractableTile>(Tiles[column * GridRows + i])->SetActorLocation(newTilePos);
			}
		}
	}

	for (auto It : FallingTilesMap)
	{
		It.Key->TempTarget = It.Key->GetActorLocation() + FVector(-TileOffset.X, 0.f, 0.f);
	}
	if (FallingTilesMap.Num() > 0)
		StartFalling();
}


void AGrid::ShuffleGrid()
{
	/** Instead of swapping the transforms why not just roll a new type? */

	//FTransform temp;
	//if (Tiles.Num() > 0)
	//{
	//	int32 LastIndex = Tiles.Num() - 1;
	//	for (int32 i = 0; i <= LastIndex; ++i)
	//	{
	//		int32 Index = FMath::RandRange(i, LastIndex);
	//		if (i != Index)
	//		{
	//			temp = Tiles[i]->GetTransform();
	//			Tiles[i]->SetActorTransform(Tiles[Index]->GetTransform());
	//			Tiles[Index]->SetActorTransform(temp);
	//		}
	//	}
	//}

	/** Only works until a new tile-type other than a Gem is introduced */
	for (ATile* tile : Tiles)
		Cast<AGemTile>(tile)->RandomizeType();
}
