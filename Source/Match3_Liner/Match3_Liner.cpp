// Copyright Epic Games, Inc. All Rights Reserved.

#include "Match3_Liner.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Match3_Liner, "Match3_Liner" );
