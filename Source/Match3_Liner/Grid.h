// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TimelineComponent.h"
#include "Grid.generated.h"

class ATile;
class AInteractableTile;

/**
 * The gird which holds the tiles and takes care of every movement within the grid
 */
UCLASS(Blueprintable)
class MATCH3_LINER_API AGrid : public AActor
{
	GENERATED_BODY()
private:
	class AMatch3_LinerGameModeBase* m_GameMode;

protected:
	// Grid- and Tile- Settings 

	/** Offset to match the static-mesh scale with the generation process */
	UPROPERTY(EditAnywhere, Category = Match3)
		FVector TileOffset = FVector(175,152,55);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Match3)
		uint8 GridRows = 8;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Match3)
		uint8 GridColumns = 9;

	// ~ Grid- and Tile- Settings 

	/** The base tiles as background*/
	UPROPERTY()
		TArray<AActor*> BaseTiles;

	/** The tiles filling the grid */
	UPROPERTY()
		TArray<ATile*> Tiles;

	/// <summary>
	/// Spawn an arbitrary tile at a specific location
	/// </summary>
	/// <param name="TileType">The tiles class</param>
	/// <param name="SpawnLocation">The spawn location</param>
	/// <returns>A pointer to the spawned Tile-Actor</returns>
	ATile* SpawnTileOfType(TSubclassOf<class ATile> TileType, FVector SpawnLocation);


	/** Used for the animation of the falling tiles. Enable in Match3_LinerGameModeBase */
	UPROPERTY(VisibleAnywhere)
		UCurveFloat* Curve;
	FTimeline FallingTimeline;
	TMap<AInteractableTile*, uint8> FallingTilesMap;

	void StartFalling();
	UFUNCTION()
		void FallingDown();
	UFUNCTION()
		void FallingFinished();

	virtual void BeginPlay() override;
	void Tick(float DeltaTime) override;

public:	
	AGrid();

	/** Initialize the tiles on the grid*/
	bool InitGrid();
	
	/// <summary>
	/// Instead of creating new tiles each game we just shuffle them.
	/// </summary>
	void ShuffleGrid();

	uint8 GetColumn(ATile* tile);
	uint8 GetRow(ATile* tile);
	/// <summary>
	/// The name is misleading, we take the "destroyed" tile and put it on top of the column and reinitialize it with another type. 
	/// After that we swap the indexes of each following tile of that column and move them one unit down.
	/// </summary>
	/// <param name="tile">Tile to "destroy"</param>
	void DeletesTileAndSpawnNew(TArray<class AInteractableTile*> tilesToDelete);
};
