// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseTile.h"

ABaseTile::ABaseTile() : ATile()
{
	PrimaryActorTick.bCanEverTick = false;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMeshComp"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundMesh(TEXT("/Game/Meshes/Hexagon_Base"));
	if (FoundMesh.Succeeded())
	{
		MeshComp->SetStaticMesh(FoundMesh.Object);
	}
	SetRootComponent(MeshComp);
	MeshComp->SetMobility(EComponentMobility::Movable);

	static ConstructorHelpers::FObjectFinder<UMaterial> FoundMaterial(TEXT("/Game/BaseMat"));
	if (FoundMaterial.Succeeded())
	{
		StoredMaterial = FoundMaterial.Object;
	}
	DynamicMaterialInst = UMaterialInstanceDynamic::Create(StoredMaterial, MeshComp);

	DynamicMaterialInst->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor::Gray);
	MeshComp->SetMaterial(0, DynamicMaterialInst);
}