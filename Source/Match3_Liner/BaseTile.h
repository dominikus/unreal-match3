// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tile.h"
#include "BaseTile.generated.h"

/**
 * The hexagonal tiles for the background without any functionality
 */
UCLASS()
class MATCH3_LINER_API ABaseTile : public ATile
{
	GENERATED_BODY()
public:
	ABaseTile();
};
